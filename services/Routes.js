/* eslint-disable no-console */
import api from './api'

export default {
    get(data) {
        return api().get(data.api)
    },
    post(data) {
        return api().post(data.api, data.obj)
    },
}
