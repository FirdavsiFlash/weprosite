import Vuex from 'vuex'
import Vue from 'vue'
import ui from './modules/ui.js'
import GetData from './modules/Data.js'


Vue.use(Vuex)

const state = () => ({
	
})

const getters = {
	ApiData: state => state.ApiData
}

const mutations = {
}

const actions = {
	VALIDATE_FORM({ commit, dispatch }, details) {
		event.preventDefault()

		let obj = {}
		let fm = new FormData(event.target)
		let need = event.target.querySelectorAll('*[data-required]').length
		let counter = 0
		let regexes = {
			name:  /^[a-z ,.'-]+$/i,
			// email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
		}

		event.target.querySelector("button").classList.add("_btn_loading")
		console.log(event.target.querySelector("button"));

		fm.forEach((value, key) => {
			let field = event.target.querySelector(`input[name=${key}]`)

			field.classList.remove('_input-error')

			if (field.getAttribute("data-required")) {
				if (!field.value.length) {
					field.classList.add('_input-error')
					counter--
				} else {
					obj[key] = value
					counter++
				}
			}

			if (field.getAttribute('data-regex')) {
				if (regexes[field.getAttribute('data-regex')].test(field.value)) {
					obj[key] = value
					counter++
				} else {
					field.classList.add('_input-error')
					counter--
				}
			}

			if (field.getAttribute('data-min')) {
				if (field.value.length > field.getAttribute('data-min')) {
					obj[key] = value
					counter++
				} else {
					field.classList.add('_input-error')
					counter--
				}
			}
		})
		console.log(counter);

		if (need <= counter){
			details.obj = obj
			details.target = event.target
			// dispatch('CLOSE_ALL_MODALS')
			dispatch('SEND_LEADS', details)
		} else {
			event.target.querySelector("button").classList.remove("_btn_loading")
			 
		}
	},
}
const modules = {
	ui,
	GetData,
	 
}
export default {
	state,
	getters,
	actions,
	mutations,
	modules
}
