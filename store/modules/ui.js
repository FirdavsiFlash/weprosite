import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = () => ({
  modals: {
    status: true,
    modal_course: {
      status: false,
    },
    modal_free_lesson: {
      status: false,
    },
    modal_contacts: {
      status: false,
    },
    modal_groups: {
      status: false,
    },
    menubar: {
      status: false,
    },
    programms: {
      status: true,
    },
  },


})

const getters = {
  modals: state => state.modals,
}

const mutations = {
  OPEN_MODAL(state, details) {
    state.modals[details.name].status = !state.modals[details.name].status
    state.modals.status = !state.modals.status
    if (process.client && process.server) {

      if (state.modals.menubar.status) {
        document.querySelector('header .links').style.display = 'none'
      } else {
        document.querySelector('header .links').style.display = 'flex'
      }

    }
    // if (state.modals[details.name].status) document.body.style.overflow = "scroll"
    // else document.body.style.overflow = "hidden"

    state.modals[details.name].text = details.text
    if (details.bg) state.modals.background.status = true

  },
  CLOSE_ALL_MODALS(state) {
    for (let item in state.modals) {
      state.modals[item].status = false
    }
  },
}

const actions = {

}

export default {
  state,
  getters,
  actions,
  mutations
}
