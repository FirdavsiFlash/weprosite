import Vuex from 'vuex'
import Vue from 'vue'
import api from '../../services/Routes'
import store from '../index'


Vue.use(Vuex)

const state = () => ({
  AllData: {
    AllGroups: {
      body: [],
      title: "AllGroups"
    },
    AllTeachers: {
      body: [],
      title: "AllTeachers"
    },
    AllCourse: {
      body: [],
      title: "AllCourse"
    },
  },
  OneElement: {},
  OneElementModals: {
    group: {},
    course: {}
  },
  ApiData: {
    AllGroups: {
      api: 'groups',
      methods: 'get'
    },
    AllCourse: {
      api: 'courses',
      methods: 'get'
    },
    AllTeachers: {
      api: 'teachers',
      methods: 'get'
    },
    OneElement: {
      api: "courses/",
      methods: 'get'
    }
  },
  AddLead: {
    leads: {
      api: "leads",
      methods: "post"
    },
    OpenLead: {
      api: "leadsOpenLesson",
      methods: "post"
    },
    Groups: {
      api: "leadsGroups",
      methods: "post"
    }
  }
})

const getters = {
  AllData: state => {
    return state.AllData
  },
  OneElementModals: state => state.OneElementModals,
  OneElement: state => state.OneElement,
  TwelveElems: state => state.TwelveElems,
  AllCourseProgram: state => state.AllCourseProgram,

}

const mutations = {
  GetData(state, data) {
    state.AllData[data.key].body = data.value.reverse()
    for (let item of state.AllData[data.key].body) {
      if (item.price) {
        item.price = item.price.toString().slice(0, 3) + ',' + item.price.toString().slice(3)
      } else if (item.courseId.price) {
        item.courseId.price = item.courseId.price.toString().slice(0, 3) + ',' + item.courseId.price.toString().slice(3)
      }
    }
    state.OneElementModals.group = state.AllData["AllGroups"].body[0]
    state.OneElementModals.course = state.AllData["AllCourse"].body[0]
  },
  GetOneElement(state, data) {
    state.OneElement = data
  },
}

const actions = {
  GetData({
    commit,
    dispatch
  }, data) {
    for (let item of data) {
      api.get({
          api: state().ApiData[item].api
        })
        .then(res => {
          if (res.status == 200 || res.status == 201 || res.status == 202 || res.status == 203 || res.status == 204) {
            commit('GetData', {
              key: item,
              value: res.data
            })
          }
        })
        .catch(error => {
          console.error(`CANT GET ELEMENTS ${item}`)
        })
    }
  },
  SEND_LEADS({
    commit,
    dispatch
  }, what) {

    let obj = {}
    let fm = new FormData(event.target)
    
    let btn = event.target.querySelector("button") 
    btn.classList.add("_btn_loading")

    fm.forEach((value, key) => {
      obj[key] = value
    })

    if (obj.alone) {
      obj.alone = true
    }

    api.post({
        api: state().AddLead[what].api,
        obj
      })
      .then(res => {
        btn.classList.remove("_btn_loading")
       commit('CLOSE_ALL_MODALS')
      })
      .catch(err =>{
        btn.classList.remove("_btn_loading")
        console.log(err);
      })

    console.log(event.target);
      },
  GetOneElement({
    commit,
    dispatch
  }) {
    let element =
      window.location.href.split("/")[
        window.location.href.split("/").length - 1
      ];
    api.get({
        api: state().ApiData.OneElement.api + element
      })
      .then(res => {
        commit("GetOneElement", res.data)
      })
      .catch(err => console.log(err))
  }
}
const modules = {}
export default {
  state,
  getters,
  actions,
  mutations,
  modules
}
