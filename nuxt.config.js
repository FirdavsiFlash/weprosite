export default {
  head: {
    title: 'Wepro',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '~/static/img/logo.png' }
    ]
  },

  publicRuntimeConfig: {
		baseURL: 'https://wepro-new-website-2022.herokuapp.com/',
	},

  css: [
    "~/static/css/library.css"
  ],

  plugins: [
    '@plugins/v-mask.js',
  ],

  components: true,

  buildModules: [
  ],

  modules: [
    '@nuxtjs/i18n',
  ],

  build: {
  },
  i18n: {
    locales: ['ru', 'uz'],
    defaultLocale: 'ru',
    vueI18n: {
      fallbackLocale: 'ru',
      messages: {
        ru: {
          state: 'ru',
          menu: 'С нуля до PRO в самой крупной IT-школе Самарканда',
          text: 'Salamlekum in USA',
          home: {
            h1: "Русский",
          }
        },
        uz: {
          state: 'uz',
          menu: 'Noldan proga qadar Samarqand shahridagi yirik it-maktab',
          text: 'Salamlekum in France',
          home: {
            h1: "Узбекский",
          }
        }
      }
    }
  },
}